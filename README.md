# Assignment1 Backend - RPG

## Install
Clone the repository.

Open the solution and remove the "Assignment1BackedTests" file that cannot be loaded.

Add a new xUnit test project by right clicking on the solution -> Add -> New Project -> xUnit Test Project. 

Name it "Assignment1BackendTests" and cut and paste the two test suite files ("CharactersTests.cs" and "EquipmentTests.cs") there instead. They are currently located in the Assignment1Backend project. 

You may delete the default test suite created by visual studio ("UnitTest1.cs").

Also, right click the test project folder ("Assignment1BackendTests")-> Add -> Project reference and add the project reference.

Had to go through this process as the test project was not seen by git and was not pushed with the solution. 

## Usage
This assignment builds a console app for an RPG game. It has three different heroes that can equip items in order to increase their attributes and deal more damage.

## Author
Sofia Vulgari
