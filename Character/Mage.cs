﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment1Backend.Equipment;
using Assignment1Backend.Exceptions;

namespace Assignment1Backend.Character
{
    public class Mage : Character, IEquip
    {

        private Dictionary<ItemSlot, Item> Equipment { get; set; }

        public Mage()
        {

        }

        /// <summary>
        /// A Mage is created. Name and level are taken from the parent constructor, PrimaryAttributes are set to correct Mage ones,
        /// TotalAttributes, a Dicionary for the Items and CharacterDps is set without items equipped.
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="level">Level</param>
        public Mage(string name, int level)
        {
            Name = name;
            Level = level;
            PrimaryAttributes = new PrimaryAttribute() { Strength = 1, Dexterity = 1, Intelligence = 8 };
            TotalPrimaryAttributes = new PrimaryAttribute();
            Equipment = new Dictionary<ItemSlot, Item>();
            CharacterDps = 1d * (1d + (Convert.ToDouble(PrimaryAttributes.Intelligence) / 100d));
        }

        /// <summary>
        /// When called the character gains level and the primaryAttributes are increased
        /// </summary>
        /// <returns>PrimaryAttributes</returns>
        public override PrimaryAttribute LevelUp()
        {
            Level++;
            PrimaryAttribute levelUpAttributes = new PrimaryAttribute() { Strength = 1, Dexterity = 1, Intelligence = 5 };
            PrimaryAttributes += levelUpAttributes;

            return PrimaryAttributes;
        }

        /// <summary>
        /// If there is an item that is a weapon the CharacterDps is calculated accordingly, else calculated without weapon.
        /// </summary>
        public void MageDps()
        {
            if (Equipment.TryGetValue(ItemSlot.WEAPON, out Item? item))
            {
                switch (item)
                {
                    case Weapon weapon:
                        CharacterDps = weapon.WeaponDps * (1d + (Convert.ToDouble(TotalPrimaryAttributes.Intelligence) / 100d));
                        break;
                    default:
                        break;
                }
            }
            else
            {
                CharacterDps = 1 * (1 + (Convert.ToDouble(TotalPrimaryAttributes.Intelligence) / 100));
            }
        }

        /// <summary>
        /// TotalAttributes are calculated. For each item that is an armor the character's strong attribute is increased by 1.
        /// </summary>
        public void getTotalAttributes()
        {
            foreach (KeyValuePair<ItemSlot, Item> item in Equipment)
            {

                switch (item.Value)
                {
                    case Armor armor:
                        TotalPrimaryAttributes.Intelligence += armor.ArmorAttributes.Intelligence;
                        TotalPrimaryAttributes.Dexterity = PrimaryAttributes.Dexterity;
                        TotalPrimaryAttributes.Strength = PrimaryAttributes.Strength;
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// A method to update the character's strong attribute, the total attributes and the DPS.
        /// Called everytime an item is equipped.
        /// </summary>
        public void update()
        {
            TotalPrimaryAttributes.Intelligence = PrimaryAttributes.Intelligence;
            getTotalAttributes();
            MageDps();
        }

        /// <summary>
        /// The weapon check. If the weapon is one that this character can equip and the level of the character is the acceptable,
        /// the item is added in the dictionary, and the update method is called. Added booleans to help with the exception handling.
        /// If the level or weapon are not correct then the equivalent exception is thrown.
        /// </summary>
        /// <param name="itemSlot">ItemSlot</param>
        /// <param name="weapon">Weapon</param>
        /// <returns>"New weapon was equipped!"</returns>
        public string EquipWeapon(ItemSlot itemSlot, Weapon weapon)
        {
            Boolean correctWeapon = false;
            Boolean correctLevel = false;

            if (weapon.WeaponEnum == WeaponEnum.STAFF || weapon.WeaponEnum == WeaponEnum.WAND)
            {
                correctWeapon = true;

                if (Level >= weapon.RequiredLevel)
                {
                    correctLevel = true;
                    Equipment.Add(itemSlot, weapon);
                }
            }

            if (!correctWeapon)
            {
                throw new InvalidWeaponException("This hero cannot equip this type of weapon.");
            }
            else if (!correctLevel)
            {
                throw new InvalidWeaponException("This hero cannot equip this level of weapon.");
            }

            update();
            string success = "New weapon was equipped!";
            return success;
        }

        /// <summary>
        /// The armor check. If the armor is one that this character can equip and the level of the character is the acceptable,
        /// the item is added in the dictionary, and the update method is called. Added booleans to help with the exception handling.
        /// If the level or armor are not correct then the equivalent exception is thrown.
        /// </summary>
        /// <param name="itemSlot">ItemSlot</param>
        /// <param name="weapon">Weapon</param>
        /// <returns>"New armor was equipped!"</returns>
        public string EquipArmor(ItemSlot itemSlot, Armor armor)
        {
            Boolean correctArmor = false;
            Boolean correctLevel = false;

            if (armor.ArmorEnum == ArmorEnum.CLOTH)
            {
                correctArmor = true;

                if (Level >= armor.RequiredLevel)
                {
                    Equipment.Add(itemSlot, armor);
                    correctLevel = true;
                }
            }

            if (!correctArmor)
            {
                throw new InvalidArmorException("This hero cannot equip this type of armor.");
            }
            else if (!correctLevel)
            {
                throw new InvalidArmorException("This hero cannot equip this level of armor.");
            }

            update();
            string success = "New armor was equipped!";
            return success;
        }
    }
}
