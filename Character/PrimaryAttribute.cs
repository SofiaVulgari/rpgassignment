﻿using System;

namespace Assignment1Backend
{
    public class PrimaryAttribute
    {
        public double Strength { get ; set; }
        public double Dexterity { get; set; }
        public double Intelligence { get; set; }

        public PrimaryAttribute()
        {
            
        }

        /// <summary>
        /// Overloading the + operator to be able to do addition of type PrimaryAttribute.
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns>PrimaryAttribute</returns>
        public static PrimaryAttribute operator +(PrimaryAttribute lhs, PrimaryAttribute rhs)
        {
            return new PrimaryAttribute 
            { 
                Strength = lhs.Strength + rhs.Strength,
                Dexterity = lhs.Dexterity + rhs.Dexterity,
                Intelligence = lhs.Intelligence + rhs.Intelligence
            };
        }
    }
}