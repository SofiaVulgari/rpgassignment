﻿using System;
using Assignment1Backend.Character;
using Assignment1Backend.Equipment;
using Assignment1Backend.Exceptions;

namespace Assignment1Backend
{
    class Program
    {
        static void Main(string[] args)
        {
            Warrior myWarrior = new Warrior("Gorbag", 1);
            myWarrior.LevelUp();
            Weapon myWeapon = new Weapon("Common axe", WeaponEnum.AXE, 2, ItemSlot.WEAPON, 7, 1.1);
            myWarrior.EquipWeapon(ItemSlot.WEAPON, myWeapon);
            Armor myHeadArmor = new Armor("Common plate head armor", ArmorEnum.PLATE, 2, ItemSlot.HEAD, 1, 0, 0);
            Armor myBodyArmor = new Armor("Common plate chest armor", ArmorEnum.PLATE, 2, ItemSlot.BODY, 1, 0, 0);
            Armor myLegsArmor = new Armor("Common plate legs armor", ArmorEnum.PLATE, 2, ItemSlot.LEGS, 1, 0, 0);
            myWarrior.EquipArmor(ItemSlot.HEAD, myHeadArmor);
            myWarrior.EquipArmor(ItemSlot.BODY, myBodyArmor);
            myWarrior.EquipArmor(ItemSlot.LEGS, myLegsArmor);
            Console.WriteLine(myWarrior.CharacterStatsDisplay());
        }
    }
}
