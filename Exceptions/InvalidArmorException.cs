﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1Backend.Exceptions
{
    public class InvalidArmorException : Exception
    {
        /// <summary>
        /// Exception for not correct armor or not correct level
        /// </summary>
        /// <param name="message"></param>
        public InvalidArmorException(string message) : base(message)
        {
        }

    }
}
