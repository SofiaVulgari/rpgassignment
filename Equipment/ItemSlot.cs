﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1Backend.Equipment
{
    /// <summary>
    /// Enum for item slots.
    /// </summary>
    public enum ItemSlot
    {
        HEAD, 
        BODY,
        LEGS,
        WEAPON
    }
}
