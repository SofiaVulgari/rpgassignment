﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1Backend.Equipment
{
    /// <summary>
    /// An enum for the different armors
    /// </summary>
    public enum ArmorEnum
    {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
    }
}
