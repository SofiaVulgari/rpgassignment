﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1Backend.Equipment
{
    /// <summary>
    /// Parent class for all items.
    /// </summary>
    public abstract class Item
    {
        public string Name { get; set; }
        public int RequiredLevel { get; set; }
        public ItemSlot ItemSlot { get; set; }

        public Item()
        {

        }

        public Item(string name, int requiredLevel, ItemSlot itemSlot)
        {
            Name = name;
            RequiredLevel = requiredLevel;
            ItemSlot = itemSlot;
        }
    }
}
