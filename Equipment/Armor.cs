﻿using System;
using Assignment1Backend.Character;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1Backend.Equipment
{
    public class Armor : Item
    {
        public PrimaryAttribute ArmorAttributes { get; set; }

        public ArmorEnum ArmorEnum { get; set; }

        public Armor()
        {

        }

        /// <summary>
        /// Armor is created with attributes of type PrimaryAttribute, the armor enum, and its name, requiredLevelel and itemSlot taken from parent. 
        /// </summary>
        /// <param name="name">string</param>
        /// <param name="armorEnum">ArmorEnum</param>
        /// <param name="requiredLevel">int</param>
        /// <param name="itemSlot">ItemSlot</param>
        /// <param name="strength">double</param>
        /// <param name="dexterity">double</param>
        /// <param name="intelligence">double</param>
        public Armor(String name, ArmorEnum armorEnum, int requiredLevel, ItemSlot itemSlot, double strength, double dexterity, double intelligence) : base(name, requiredLevel, itemSlot)
        {
            ArmorEnum = armorEnum;
            ArmorAttributes = new PrimaryAttribute() { Strength = strength,  Dexterity = dexterity, Intelligence = intelligence};
        }
    }
}
