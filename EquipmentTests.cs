﻿using System;
using Xunit;
using Assignment1Backend.Equipment;
using Assignment1Backend.Character;
using Assignment1Backend.Exceptions;

namespace Assignment1BackendTests
{
    public class EquipmentTests
    {
        Warrior testWarrior = new Warrior("Gorbag", 1);

        
        [Fact]
         public void EquipWeapon_TriesToEquipHigherLevelWeapon_InvalidWeaponExceptionShouldBeThrown()
         {
            //Arrange
            Weapon testWeapon = new Weapon("Common Axe", WeaponEnum.AXE, 2, ItemSlot.WEAPON, 7, 1.1);

            //Assert
            Assert.Throws<InvalidWeaponException>(() => testWarrior.EquipWeapon(ItemSlot.WEAPON, testWeapon));
        }

        [Fact]
        public void EquipArmor_TriesToEquipHigherLevelArmor_InvalidArmorExceptionShouldBeThrown()
        {
            //Arrange
            Armor testArmor = new Armor("Common plate body armor", ArmorEnum.PLATE, 2, ItemSlot.BODY, 1, 0, 0);

            //Assert
            Assert.Throws<InvalidArmorException>(() => testWarrior.EquipArmor(ItemSlot.BODY, testArmor));
        }

        [Fact]
        public void EquipWeapon_TriesToEquipWrongWeaponType_InvalidWeaponExceptionShouldBeThrown()
        {
            //Arrange
            Weapon testWeapon = new Weapon("Common Bow", WeaponEnum.BOW, 2, ItemSlot.WEAPON, 12, 0.8);
            testWarrior.LevelUp();

            //Assert
            Assert.Throws<InvalidWeaponException>(() => testWarrior.EquipWeapon(ItemSlot.WEAPON, testWeapon));
        }

        [Fact]
        public void EquipArmor_TriesToEquipWrongArmorType_InvalidArmorExceptionShouldBeThrown()
        {
            //Arrange
            Armor testArmor = new Armor("Common cloth head armor", ArmorEnum.CLOTH, 2, ItemSlot.HEAD, 0, 0, 5);
            testWarrior.LevelUp();

            //Assert
            Assert.Throws<InvalidArmorException>(() => testWarrior.EquipArmor(ItemSlot.BODY, testArmor));
        }

        [Fact]
        public void EquipWeapon_EquipsCorrectWeapon_SuccessfulMessageShouldBeReturned()
        {
            //Arrange
            Weapon testWeapon = new Weapon("Common Axe", WeaponEnum.AXE, 2, ItemSlot.WEAPON, 7, 1.1);
            testWarrior.LevelUp();
            string expected = "New weapon was equipped!";
            string actual = testWarrior.EquipWeapon(ItemSlot.WEAPON, testWeapon);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipArmor_EquipsCorrectArmor_SuccessfulMessageShouldBeReturned()
        {
            //Arrange
            Armor testArmor = new Armor("Common plate body armor", ArmorEnum.PLATE, 2, ItemSlot.BODY, 1, 0, 0);
            testWarrior.LevelUp();
            string expected = "New armor was equipped!";
            string actual = testWarrior.EquipArmor(ItemSlot.BODY, testArmor);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CharacterDps_CalculateDamageWhithNoWeapon_ShouldBeSameAsExpected()
        {
            //Arrange
            double expected = 1d * (1d + (5d / 100d));
            double actual = testWarrior.CharacterDps;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CharacterDps_CalculateDamageWhithValidWeapon_ShouldBeSameAsExpected()
        {
            //Arrange
            Weapon testWeapon = new Weapon("Common Axe", WeaponEnum.AXE, 1, ItemSlot.WEAPON, 7, 1.1);
            testWarrior.EquipWeapon(ItemSlot.WEAPON, testWeapon);
            double expected = (7d * 1.1d) * (1d + (5d / 100d));
            double actual = testWarrior.CharacterDps;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CharacterDps_CalculateDamageWhithValidWeaponAndArmor_ShouldBeSameAsExpected()
        {
            //Arrange
            Weapon testWeapon = new Weapon("Common Axe", WeaponEnum.AXE, 1, ItemSlot.WEAPON, 7, 1.1);
            Armor testArmor = new Armor("Common plate body armor", ArmorEnum.PLATE, 1, ItemSlot.BODY, 1, 0, 0);
            testWarrior.EquipWeapon(ItemSlot.WEAPON, testWeapon);
            testWarrior.EquipArmor(ItemSlot.BODY, testArmor);
            
            double expected = (7d * 1.1d) * (1d + ((5d + 1d) / 100d)); 

            double actual = testWarrior.CharacterDps;
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
