using Assignment1Backend.Character;
using System;
using Xunit;

namespace Assignment1BackendTests
{
    public class CharactersTests
    {

        #region Initialization
        [Fact]
        public void Warrior_OnInitialized_ShouldBeLevel1()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Gorbag", 1);
            int level = 1;
            int expected = level;
            int actual = testWarrior.Level;

            //Assert
            Assert.Equal(expected, actual);
        }



        [Theory]
        [InlineData(5, 2, 1)]
        public void Warrior_OnInitialized_ShouldHaveCorrectLevelAttributes(double expectedStrength, double expectedDexterity, double expectedIntelligence)
        {
            //Arrange
            Warrior testWarrior = new Warrior("Gorbag", 1);
            double actualStrength = testWarrior.PrimaryAttributes.Strength;
            double actualDexterity = testWarrior.PrimaryAttributes.Dexterity;
            double actualIntelligence = testWarrior.PrimaryAttributes.Intelligence;

            //Act
            Boolean checkIfEqual() {
                bool areEqual;
                if (actualStrength == expectedStrength && actualDexterity == expectedDexterity && actualIntelligence == expectedIntelligence)
                {
                    areEqual = true;
                }
                else areEqual = false;

                return areEqual;
            }

            //Assert
            
            Assert.True(checkIfEqual());
        }

        [Theory]
        [InlineData(1, 1, 8)]
        public void Mage_Initialized_ShouldHaveCorrectLevelAttributes(double expectedStrength, double expectedDexterity, double expectedIntelligence)
        {
            //Arrange
            Mage testMage = new Mage("Gandalf", 1);
            double actualStrength = testMage.PrimaryAttributes.Strength;
            double actualDexterity = testMage.PrimaryAttributes.Dexterity;
            double actualIntelligence = testMage.PrimaryAttributes.Intelligence;

            //Act
            Boolean checkIfEqual()
            {
                bool areEqual;
                if (actualStrength == expectedStrength && actualDexterity == expectedDexterity && actualIntelligence == expectedIntelligence)
                {
                    areEqual = true;
                }
                else areEqual = false;

                return areEqual;
            }

            //Assert
            Assert.True(checkIfEqual());
        }

        [Theory]
        [InlineData(1, 7, 1)]
        public void Ranger_Initialized_ShouldHaveCorrectLevelAttributes(double expectedStrength, double expectedDexterity, double expectedIntelligence)
        {
            //Arrange
            Ranger testRanger = new Ranger("Legolas", 1);
            double actualStrength = testRanger.PrimaryAttributes.Strength;
            double actualDexterity = testRanger.PrimaryAttributes.Dexterity;
            double actualIntelligence = testRanger.PrimaryAttributes.Intelligence;


            //Act
            Boolean checkIfEqual()
            {
                bool areEqual;
                if (actualStrength == expectedStrength && actualDexterity == expectedDexterity && actualIntelligence == expectedIntelligence)
                {
                    areEqual = true;
                }
                else areEqual = false;

                return areEqual;
            }

            //Assert
            Assert.True(checkIfEqual());
        }

        [Theory]
        [InlineData(2, 6, 1)]
        public void Rogue_Initialized_ShouldHaveCorrectLevelAttributes(double expectedStrength, double expectedDexterity, double expectedIntelligence)
        {
            //Arrange
            Rogue testRogue = new Rogue("Aragorn", 1);
            double actualStrength = testRogue.PrimaryAttributes.Strength;
            double actualDexterity = testRogue.PrimaryAttributes.Dexterity;
            double actualIntelligence = testRogue.PrimaryAttributes.Intelligence;

            //Act
            Boolean checkIfEqual()
            {
                bool areEqual;
                if (actualStrength == expectedStrength && actualDexterity == expectedDexterity && actualIntelligence == expectedIntelligence)
                {
                    areEqual = true;
                }
                else areEqual = false;

                return areEqual;
            }

            //Assert
            Assert.True(checkIfEqual());
        }
        #endregion

        #region LevelUp
        [Fact]
        public void Warrior_OnLevelUp_ShouldBeLevel2()
        {
            //Arrange
            int level = 2;
            int expected = level;
            Warrior testWarrior = new Warrior("Gorbag", 1);
            testWarrior.LevelUp();
            int actual = testWarrior.Level;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(8, 4, 2)]
        public void Warrior_OnLevelUp_ShouldHaveCorrectlyIncreasedAttributes(double expectedStrength, double expectedDexterity, double expectedIntelligence)
        {
            //Arrange
            Warrior testWarrior = new Warrior("Gorbag", 1);
            testWarrior.LevelUp();
            double actualStrength = testWarrior.PrimaryAttributes.Strength;
            double actualDexterity = testWarrior.PrimaryAttributes.Dexterity;
            double actualIntelligence = testWarrior.PrimaryAttributes.Intelligence;

            //Act
            Boolean checkIfEqual()
            {
                bool areEqual;
                if (actualStrength == expectedStrength && actualDexterity == expectedDexterity && actualIntelligence == expectedIntelligence)
                {
                    areEqual = true;
                }
                else areEqual = false;

                return areEqual;
            }

            //Assert
            Assert.True(checkIfEqual());
        }

        [Theory]
        [InlineData(2, 2, 13)]
        public void Mage_LevelsUp_ShouldHaveCorrectlyIncreasedAttributes(double expectedStrength, double expectedDexterity, double expectedIntelligence)
        {
            //Arrange
            Mage testMage = new Mage("Gandalf", 1);
            testMage.LevelUp();
            double actualStrength = testMage.PrimaryAttributes.Strength;
            double actualDexterity = testMage.PrimaryAttributes.Dexterity;
            double actualIntelligence = testMage.PrimaryAttributes.Intelligence;

            //Act
            Boolean checkIfEqual()
            {
                bool areEqual;
                if (actualStrength == expectedStrength && actualDexterity == expectedDexterity && actualIntelligence == expectedIntelligence)
                {
                    areEqual = true;
                }
                else areEqual = false;

                return areEqual;
            }

            //Assert
            Assert.True(checkIfEqual());
        }

        [Theory]
        [InlineData(3, 10, 2)]
        public void Rogue_LevelsUp_ShouldHaveCorrectlyIncreasedAttributes(double expectedStrength, double expectedDexterity, double expectedIntelligence)
        {
            //Arrange
            Rogue testRogue = new Rogue("Aragorn", 1);
            testRogue.LevelUp();
            double actualStrength = testRogue.PrimaryAttributes.Strength;
            double actualDexterity = testRogue.PrimaryAttributes.Dexterity;
            double actualIntelligence = testRogue.PrimaryAttributes.Intelligence;

            //Act
            Boolean checkIfEqual()
            {
                bool areEqual;
                if (actualStrength == expectedStrength && actualDexterity == expectedDexterity && actualIntelligence == expectedIntelligence)
                {
                    areEqual = true;
                }
                else areEqual = false;

                return areEqual;
            }

            //Assert
            Assert.True(checkIfEqual());
        }

        [Theory]
        [InlineData(2, 12, 2)]
        public void Ranger_LevelsUp_ShouldHaveCorrectlyIncreasedAttributes(double expectedStrength, double expectedDexterity, double expectedIntelligence)
        {
            //Arrange
            Ranger testRanger = new Ranger("Legolas", 1);
            testRanger.LevelUp();
            double actualStrength = testRanger.PrimaryAttributes.Strength;
            double actualDexterity = testRanger.PrimaryAttributes.Dexterity;
            double actualIntelligence = testRanger.PrimaryAttributes.Intelligence;

            //Act
            Boolean checkIfEqual()
            {
                bool areEqual;
                if (actualStrength == expectedStrength && actualDexterity == expectedDexterity && actualIntelligence == expectedIntelligence)
                {
                    areEqual = true;
                }
                else areEqual = false;

                return areEqual;
            }

            //Assert
            Assert.True(checkIfEqual());
        }
        #endregion
    }
}
